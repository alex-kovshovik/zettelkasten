# 2021-09-16 Tailwind CSS Beautiful Box Shadows

In a recent article by Josh W Comeau [Designing Beautiful Shadows in CSS](https://www.joshwcomeau.com/css/designing-shadows/) he outlines how to create layered shadows in order to create a more real-world layered shadow.  

TailwindCSS [Tailwind CSS Box Shadow](https://tailwindcss.com/docs/box-shadow) uses a layered shadow approach to box shadows.  No additional configuration needed.

---

#ux #tailwindcss #shadow #shadows

---

- [Designing Beautiful Shadows in CSS](https://www.joshwcomeau.com/css/designing-shadows/)
- [Smoother and Sharper Shadows with Layered box-shadow](https://tobiasahlin.com/blog/layered-smooth-box-shadows/)
- [shadows.brumm.af](https://shadows.brumm.af/) - tool to generate layered shadows
- [Box Shadow - Tailwind CSS](https://tailwindcss.com/docs/box-shadow)
- [2021-09-17 Tailwind CSS Group Hover](2021-09-17 tailwind css group hover.md) 

