# 2021-09-17 Tailwind CSS Group Hover Mobile

TailwindCSS [group hover](2021-09-17 tailwind css group hover.md) is a convenient utility class, but unfortunately it does not work well on devices which do not support hover (e.g. mobile phones, tablets, etc).  Using group-hover on some touch devices will then require a double touch (1st touch to show group-hover state, 2nd touch to execute action).  You can test this in chrome using the mobile view in developer tools.

Using a CSS media query for size (sm, md, large) can solve for mobile phones, but not for larger tablets.  

The correct solution is to use CSS to test if the device support hover using a [hover media query](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/hover).  We added a new media queries in our TailwindCSS config to support these. 

```diff
      screens: {
        print: { raw: 'print' },
        screen: { raw: 'screen' },
+       'media-hover': { raw: '(hover: hover)' },
+       'media-hover-none': { raw: '(hover: none)' },
      },

```

To use these, you would change:

```diff
<div class="group">
-  <p class="text-indigo-600 group-hover:text-gray-900">New Project</p>
+  <p class="text-indigo-600 media-hover:group-hover:text-gray-900">New Project</p>
-  <p class="text-indigo-500 group-hover:text-gray-500">Create a new project.</p>
+  <p class="text-indigo-500 media-hover:group-hover:text-gray-500">Create a new project.</p>
</div>
```

or change the root group if hover is the only item used in group

```diff
- <div class="group">
+ <div class="media-hover:group">
  <p class="text-indigo-600 group-hover:text-gray-900">New Project</p>
  <p class="text-indigo-500 group-hover:text-gray-500">Create a new project.</p>
</div>
```

In a recent case, we also used `media-hover-none` to automatically set the state of a hover for non-hover devices.

I'm afraid we won't catch this in the future and wonder if we could add an HTML attribute linter to catch these.  It would be nice if we could set the media query as the default for group-hover, but for now using 

---

#ux #tailwindcss #hover #group-hover #mobile #touch

---

- [TailwindCSS group-hover](https://tailwindcss.com/docs/hover-focus-and-other-states#group-hover)
- [2021-09-17 Tailwind CSS Group Hover](2021-09-17 tailwind css group hover.md) 
- [hover - CSS: Cascading Style Sheets | MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/hover)
- [Disabling mobile styles on hover? · Discussion #1560 · tailwindlabs/tailwindcss · GitHub](https://github.com/tailwindlabs/tailwindcss/discussions/1560)
- [Add support for media pointer · Discussion #4022 · tailwindlabs/tailwindcss · GitHub](https://github.com/tailwindlabs/tailwindcss/discussions/4022)
- [Hover states on phones · Discussion #1998 · tailwindlabs/tailwindcss · GitHub](https://github.com/tailwindlabs/tailwindcss/discussions/1998)
- [[Feature discussion] Add variants through plugins · Issue #496 · tailwindlabs/tailwindcss · GitHub](https://github.com/tailwindlabs/tailwindcss/issues/496)

