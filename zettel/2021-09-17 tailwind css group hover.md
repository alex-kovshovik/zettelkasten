# 2021-09-17 Tailwind CSS Group Hover

Just like TailwindCSS includes out-of-the-box support for [Layered CSS Box Shadow](https://tailwindcss.com/docs/box-shadow), it also includes out-of-the-box [group hover](https://tailwindcss.com/docs/hover-focus-and-other-states#group-hover).  With group-hover you can style a child element while hovering over a parent element.  A powerful utility class.

---

#ux #tailwindcss #hover #group-hover

---

- [TailwindCSS group-hover](https://tailwindcss.com/docs/hover-focus-and-other-states#group-hover)
- [2021-09-16 Tailwind CSS Beautiful Box Shadows](2021-09-16 tailwind css beautiful box shadows.md) 
- [2021-09-17 tailwind css group hover mobile](2021-09-17 tailwind css group hover mobile.md) 

