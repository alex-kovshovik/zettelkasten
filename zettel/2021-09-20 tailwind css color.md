# 2021-09-20 Tailwind CSS Color

TailwindCSS includes a palette [8 color gradients out-of-the-box](https://tailwindcss.com/docs/customizing-colors) that can be used background, text, border, ring and hopfully soon [shadow](2021-09-20 tailwind css shadow color.md)  colors.  You can increase the palette to 22 if you [import](https://tailwindcss.com/docs/customizing-colors#color-palette-reference) `tailwindcss/colors` into `tailwind.config.js`.  You create your own color gradients to match with your sites theme.

Unused colors will be removed during Tailwinds [tree-shaking](https://tailwindcss.com/docs/optimizing-for-production).

---

#ux #tailwindcss #color

---

- [Customizing Colors - Tailwind CSS](https://tailwindcss.com/docs/customizing-colors)
- [Why Color Theory Sucks (& what framework to use instead)](https://learnui.design/blog/color-in-ui-design-a-practical-framework.html)
- [2021-09-20 tailwind css shadow color](2021-09-20 tailwind css shadow color.md) 
- [2021-09-20 black in nature](2021-09-20 black in nature.md) 

