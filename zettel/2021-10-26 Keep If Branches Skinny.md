# 2021-10-26 Keep If Branches Skinny

It's general knowledge that "Code is read more often than it is written".  When writing complex if/else conditionals, one such technique revolves around keeping if branches skinny.

The following code adds some cognitive load to the reader.

```ruby
def comment_on_popularity
  if !post.stars.empty?
      if post.stars.count > 100
          "Congratulations. Your post is very popular."
      elsif post.stars.count > 50
          "Well done. Your post is trending."
      else
          "Your post is doing well but could use a little attention."
      end
  else
      "Your post is flying under the radar."
  end
end
```

By the time you get to the else branch, you need to keep all the if information in memory.  This adds cognitive load to the reader.  You can decrease the cognitive load by refactoring with a skinny *if* branch and branch to the right.

```ruby
def comment_on_popularity
  if post.stars.empty?
      "Your post is flying under the radar."
  else
      if post.stars.count > 100
          "Congratulations. Your post is popular!"
      elsif post.stars.count > 50
          "Well done. Your post is trending."
      else
          "Doing well but could use a little attention."
      end
  end
end
```

---

#ruby #javascript #readability

---

- [Let’s Get Serious About Readability](https://medium.com/newsonthebloc/lets-get-serious-about-readability-4e4ce6a9b6c2)
