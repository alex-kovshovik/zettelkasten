# 2021-11-04 Cons of Keyset Pagination

Seek/Keyset pagination [performs better than offset pagination](2021-11-04 Avoid SQL OFFSET.md), but nothing comes for free.  There are few cons.

## Page Specificity

With keyset pagination, there are no page numbers.  You can calculate the number of pages (records / limit per page), but you can't show the user "page 3 of 10" or offer the user to "jump to page [5]".  You can only provide the user with Next, Previous, First Page, Last Page.

From a UX perspective, it's not helpful to see "Page 1 of 3,280"?  And your user is not likely to "jump to page 2,345".  It can be useful on an eCommerce page with smaller maximum records in a category, but with a large lifetime maximum result set for the user it's not a good approach.

## Order Indexes Required

The performance of keyset pagination depends on using a database index and the number of `ORDER BY` columns.  From my perspective, this just means it's not magic and developers need to understand how it works and architect add the proper indexes accordingly.

## UX Solutions

The goal for pagination is to help the user find records.  For large datasets, search/filter tools provide a better experience for users to find what they need.

---

#ruby #sql #pagination #performance

---

- [2021-11-04 Avoid SQL OFFSET.md](2021-11-04 Avoid SQL OFFSET.md) 
- [Keyset pagination | GitLab](https://docs.gitlab.com/ee/development/database/keyset_pagination.html)
- [Avoid the Pains of Pagination](https://uxmovement.com/navigation/avoid-the-pains-of-pagination/)
- [Pagination Best Practices for Google | Google Search Central](https://developers.google.com/search/docs/advanced/ecommerce/pagination-and-incremental-page-loading)

