# 2021-12-10 iOS SVG Display Issue

iOS does not display SVG without width and height attribute.

## iOS Safari/Chrome does not display

```erb
<%= inline_svg_tag('svg/my-svg.svg', title: 'My SVG') %>
```
outputs and does NOT display
```html
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">...</svg>
```

## Add width/height attribute

```erb
<%= inline_svg_tag('svg/my-svg.svg', title: 'My SVG', size: '40px*40px') %>
```
outputs and displays
```html
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="40px" height="40px">...</svg>
```
---

#ruby #svg

---

- [javascript - iOS not displaying SVG in <img> tag - Stack Overflow](https://stackoverflow.com/questions/22591605/ios-not-displaying-svg-in-img-tag)

