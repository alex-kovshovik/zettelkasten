# 2021-12-28 ActiveRecord::Relation touch_all

In using a Russian doll caching strategy, you want to update related records to reflect the update occurred using `#touch`.

```ruby
class Brake < ActiveRecord::Base
  belongs_to :car, touch: true
end
```

In some scenarios, this does not work:

- Relationships other than `belongs_to` (e.g. `has_many`)
- Updating records in batch which skips callbacks (e.g. `#update_all`)

For these scenarios you can touch the records individually.

```ruby
car.wheels.each(&:touch)
```

However the above runs a separate query for each record.  It can be more efficient to update them all in a single query:

```ruby
car.wheels.update_all(updated_at: Time.now))
```

Rails 6 provides a more convenient method to do the above with `#touch_all`

```ruby
car.wheels.touch_all
```

### Warning: Skipping Callbacks

As mentioned earlier, batch query methods like `#update_all` will skip callbacks.  This includes   `#touch_all` .  So if you have additional callbacks assigned to `wheels` in this case, they will not be executed and you may want to consider using `#each(&:touch)` or manually run non-executed callback methods.


---

#ruby #rails #caching

---

- [Rails 6 adds touch_all method to ActiveRecord::Relation | Saeloun Blog](https://blog.saeloun.com/2019/08/27/rails-6-touch_all-method.html)
- [touch_all (ActiveRecord::Relation) - APIdock](https://apidock.com/rails/ActiveRecord/Relation/touch_all)

