# 2022-01-03 Simplify View String Interpolation with Rails ActionView::Helpers::TagHelper

With Hotwired data attributes, html string interpolation can become messy and less readable.

```erb
<div id="<%= dom_id(@images, :list) %>"
     data-controller="lightbox"
     data-lightbox-options-value='{"controls": <%= @images.controls %>, "loop": <%= @images.loop %>}'>
  ...
</div>
```

In some scenarios, this does not work:

Change to use the `#tag` helper.

```erb
<%= tag.div id: dom_id(@images, :list),
            data: {
                controller: 'lightbox',
                lightbox_options_value: { controls: @images.controls, loop: @images.loop },
              } do %>
  ...
<% end %>
```

Note: if an attribute is `nil`, the attribute is excluded from the HTML output:

```erb
<%= tag.span id: nil, data: { controller: 'lightbox' } do %>
  ...
<% end %>
```

Outputs html without an id attribute

```html
<span data-controller="lightbox">
  ...
</span>
```



---

#ruby #rails #actionview #erb

---

- 

