# 2022-02-01 More Rails Controllers, Less Stimulus Controllers

We should be writing more Rails controllers (a lot more), and less Stimulus controllers.

When thinking of a UI experience, we immediately reach to JavaScript.  However, what if JavaScript didn't exist.  What if we could only code in html, css and Rails.  This is how we should think.  Stretch as far as you can with Rails, and when you can't go any further add a little JavaScript as possible until you can write more Rails.  Rinse and repeat.?

#### Example: Create a form where a checkbox toggles part of the form.  For a new record the checkbox is unchecked by default.  If the user checks the box, then hide part of the form.

**Solution:** instead of creating Stimulus controller to toggle the hidden parts, consider a Stimulus controller that submits the unfinished form to the existing Rails controller which uses attributes to hide a part of the form. 

#### Example: Create a form where a input select#change populates default values in additional input fields in form.

**Solution:** instead of creating Stimulus controller to populate the input fields based on meta data applied to each select option, consider a Stimulus controller that submits the unfinished form on #change to the existing Rails controller which uses attributes to populate input fields on the form.  

In both scenarios, you likely need to create this backend code anyway to 1) render an invalid form and 2) render an #edit form.  Now the hidden logic is one place.  Also in both scenarios, a more specific stimulus controller solution would be different controllers (one to show/hide part of the form and another to populate fields).  Instead, we now have one simple rails controller which "submits" an unfinished form.  And the user can't tell the difference.

### What about server resources?

We often hesitate with this type of solution wanting to overload backend resources for simple interactions>=. However, server resources are becoming cheaper, while bugs and duplicate business logic is expensive.  Also, using simple Rails controllers with simple/small DB queries, good caching, and the Hotwire framework simplifies the creation and maintenance of the code. 

## More Rails Controllers, Less Stimulus Controllers!

---

#rails #stimulus

---

- [Rails Controllers](https://guides.rubyonrails.org/action_controller_overview.html)
- [Stimulus Controllers](https://stimulus.hotwired.dev/reference/controllers)

