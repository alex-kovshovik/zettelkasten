# 2022-02-09 ActionView CaptureHelper

When rendering an ERB, sometimes you only want to render sections based on conditionals.  However, the conditionals can get complicated.  For example:

```erb
<% if menu_item_1? || menu_item_1? || menu_item_3? || menu_item_4? %>
  <h1>My Menu</h1>
  <ul>
    <%= content_tag(:li, "Item 1") if menu_item_1? %>
    <%= content_tag(:li, "Item 2") if menu_item_2? %>
    <%= content_tag(:li, "Item 3") if menu_item_3? %>
    <%= content_tag(:li, "Item 4") if menu_item_4? %>
  </ul>
<% end %>
```

In the above example, our conditional logic is duplicated twice and every time we add a new item to the menu, we need to add the conditional in two places.

Rails includes a [CaptureHelper](https://apidock.com/rails/ActionView/Helpers/CaptureHelper) class with ERB helper methods to capture and then later render HTML.

## CaptureHelper#capture 

Using Rails CaptureHelper [#capture](https://apidock.com/rails/v5.2.3/ActionView/Helpers/CaptureHelper/capture) helper, we can save the rendered HTML inside a block to a variable and then user later.  

```erb
<% my_menu = capture do %>
  <%= content_tag(:li, "Item 1") if menu_item_1? %>
  <%= content_tag(:li, "Item 2") if menu_item_2? %>
  <%= content_tag(:li, "Item 3") if menu_item_3? %>
  <%= content_tag(:li, "Item 4") if menu_item_4? %>
<% end %>

<% if my_menu.present? %>
  <h1>My Menu</h1>
  <ul>
    <%= my_menu %>
  </ul>
<% end %>
```

NOTE:  pay attention when using `<%` vs `<%=` for a capture block. With the equals sign `<%= my_menu = capture do %>` will both save the content to memory, but will also print the output to the browser at the time of capture.  This is often not wanted as you intend to output the HTML later.

## CaptureHelper #content_for / content_for?

With Rails CaptureHelper [#content_for](https://apidock.com/rails/ActionView/Helpers/CaptureHelper/content_for) and [#content_for?](https://apidock.com/rails/v5.2.3/ActionView/Helpers/CaptureHelper/content_for%3F) helpers we can do the same as above.  

```erb
<% content_for :my_menu do %>
  <%= content_tag(:li, "Item 1") if menu_item_1? %>
  <%= content_tag(:li, "Item 2") if menu_item_2? %>
  <%= content_tag(:li, "Item 3") if menu_item_3? %>
  <%= content_tag(:li, "Item 4") if menu_item_4? %>
<% end %>

<% if content_for? :my_menu %>
  <h1>My Menu</h1>
  <ul>
    <%= yield :my_menu %>
  </ul>
<% end %>
```

#content_for? checks to see if the string inside of the buffer is `#present?`

```ruby
# actionview/lib/action_view/helpers/capture_helper.rb
def content_for?(name)
  @view_flow.get(name).present?
end
```

## Scope

Note that #content_for saves content to a global object scope where other emplates or partials can have access to the content (be careful in naming conflicts), where capture only saves to the variable scope which you saved the output to.

---

#rails #erb

---

- [CaptureHelper](https://apidock.com/rails/ActionView/Helpers/CaptureHelper) 
- [content_for](https://apidock.com/rails/ActionView/Helpers/CaptureHelper/content_for) 
- [content_for?](https://apidock.com/rails/v5.2.3/ActionView/Helpers/CaptureHelper/content_for%3F)
