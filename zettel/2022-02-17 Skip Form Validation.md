

# 2022-02-17 Dynamically Disable Form Validation using HTML

In the scenario where we use [formaction or formmethod](2022-02-16 Dynamically change Form Action or Method using HTML.md) to change how we submit the form, we may run into form validation issues.  For example, when submitting a form, we may require certain fields... however when searching for similar issues of the form we might not necessarily require validation.

```html
<form action="/issues" method="POST">
  <input type="text" id="title" name="title" required>
  <!-- your form inputs here -->

  <input type="submit" value="Submit" />
  <input type="submit" value="Search for similar issues" formaction="/issues/search" formmethod="GET">
</form>
```

Both buttons would run HTML form validations and would prevent the form from submitting if invalid.

## form[novalidate] attribute
We could use Javascript to dynamically add [novalidate](https://www.w3schools.com/tags/att_form_novalidate.asp) to the form in order to skip validation.

```html
<form action="/issues" method="POST">
  <input type="text" id="title" name="title" required>
  <!-- your form inputs here -->

  <input type="submit" value="Submit" />
  <input type="submit" value="Search for similar issues" onclick="addNovalidateForForm">
</form>
```

However, there's a native HTML, non-javascript way to accomplish this. 🎉

## [formnovalidate] attribute

You can do this with just HTML using attribute [formnovalidate](https://www.w3schools.com/tags/att_input_formnovalidate.asp) on `input` or `button` elements.

```diff
<form action="/issues" method="POST">
  <input type="text" id="title" name="title" required>
  <!-- your form inputs here -->

  <input type="submit" value="Submit" />
- <input type="submit" value="Search for similar issues" onclick="addNovalidateForForm">
+ <input type="submit" value="Search for similar issues" formnovalidate>
</form>
```


---

#html #forms

---

-  [2022-02-16 Dynamically change Form Action or Method using HTML.md](2022-02-16 Dynamically change Form Action or Method using HTML.md) 
- [HTML form novalidate Attribute](https://www.w3schools.com/tags/att_form_novalidate.asp)
